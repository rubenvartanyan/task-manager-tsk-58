# TASK MANAGER

Console application

# DEVELOPER INFO

NAME: Ruben Vartanyan

E-MAIL: rvartanyan@tsconsulting.com

# SOFTWARE

- Windows 10
- JDK 1.8

# HARDWARE 

- CPU i7
- RAM 16GB

# BUILD PROGRAM

```
mvn clean install
```

# RUN PROGRAM

```
java -jar task-manager.jar
```

# SCREENSHOTS

https://yadi.sk/d/FF_-y7DEIuEhrA?w=1
