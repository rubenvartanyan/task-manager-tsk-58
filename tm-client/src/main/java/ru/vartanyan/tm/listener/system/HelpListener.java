package ru.vartanyan.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.event.ConsoleEvent;
import ru.vartanyan.tm.listener.AbstractListener;

@Component
public class HelpListener extends AbstractListener {

    @NotNull
    @Autowired
    private AbstractListener[] listeners;


    @Override
    public @Nullable String arg() {
        return "-h";
    }

    @Override
    public @NotNull String name() {
        return "help";
    }

    @Override
    public @Nullable String description() {
        return "Display list of terminal listeners";
    }

    @Override
    @EventListener(condition = "@helpListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        System.out.println("[HELP]");
        for (@NotNull final AbstractListener listener : listeners) {
            System.out.println(listener);
        }
    }

}
