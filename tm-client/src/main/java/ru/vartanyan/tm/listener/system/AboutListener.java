package ru.vartanyan.tm.listener.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.event.ConsoleEvent;
import ru.vartanyan.tm.listener.AbstractListener;

@Component
public class AboutListener extends AbstractListener {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public @NotNull String name() {
        return "show-about";
    }

    @Override
    public String description() {
        return "Show about developer info";
    }

    @Override
    @EventListener(condition = "@aboutListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        System.out.println("[ABOUT]");
        System.out.println(propertyService.getDeveloperName());
        System.out.println(propertyService.getDeveloperEmail());
    }

}
