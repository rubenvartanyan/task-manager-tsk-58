package ru.vartanyan.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.vartanyan.tm.endpoint.AdminEndpoint;

public abstract class AbstractUserListener extends AbstractListener {

    @NotNull
    @Autowired
    public AdminEndpoint adminEndpoint;

}
