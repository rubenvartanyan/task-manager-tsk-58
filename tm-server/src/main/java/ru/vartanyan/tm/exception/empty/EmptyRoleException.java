package ru.vartanyan.tm.exception.empty;

public class EmptyRoleException extends Exception{

    public EmptyRoleException() {
        super("Error! Role cannot be null or empty...");
    }

}
