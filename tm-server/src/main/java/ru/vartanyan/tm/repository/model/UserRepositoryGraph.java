package ru.vartanyan.tm.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.vartanyan.tm.api.repository.model.IUserRepositoryGraph;
import ru.vartanyan.tm.model.UserGraph;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Scope("prototype")
public class UserRepositoryGraph extends AbstractRepository<UserGraph>
        implements IUserRepositoryGraph {


    @NotNull
    public List<UserGraph> findAll() {
        return entityManager.createQuery("SELECT e FROM UserGraph e", UserGraph.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    public @Nullable UserGraph findOneById(@Nullable final String id) {
        return entityManager.find(UserGraph.class, id);
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM UserGraph e")
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    public void removeOneById(@Nullable final String id) {
        UserGraph reference = entityManager.getReference(UserGraph.class, id);
        entityManager.remove(reference);
    }

    @Override
    public @Nullable UserGraph findByLogin(@Nullable final String login) {
        List<UserGraph> list = entityManager
                .createQuery("SELECT e FROM UserGraph e WHERE e.login = :login", UserGraph.class)
                .setParameter("login", login)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1).getResultList();
        if (list.size() != 0) {
            return list.get(0);
        }
        else {
            return null;
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        entityManager
                .createQuery("DELETE FROM UserGraph e WHERE e.login = :login")
                .setParameter("login", login)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

}

