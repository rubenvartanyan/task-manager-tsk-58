package ru.vartanyan.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.endpoint.IProjectEndpoint;
import ru.vartanyan.tm.api.service.dto.IProjectService;
import ru.vartanyan.tm.api.service.dto.IProjectTaskService;
import ru.vartanyan.tm.api.service.dto.ISessionService;
import ru.vartanyan.tm.api.service.dto.ITaskService;
import ru.vartanyan.tm.dto.Project;
import ru.vartanyan.tm.dto.Session;
import ru.vartanyan.tm.exception.system.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private IProjectTaskService projectTaskService;

    @Override
    @WebMethod
    public Project addProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AccessDeniedException {
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        return projectService.add(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    public void clearBySessionProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws AccessDeniedException{
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        if (session.getUserId() == null) throw new AccessDeniedException();
        projectService.clear(session.getUserId());
    }

    @Override
    @NotNull
    @WebMethod
    public List<Project> findProjectAll(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws AccessDeniedException{
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        return projectService.findAll(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public Project findProjectOneById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException {
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        return projectService.findOneById(session.getUserId(), id);
    }

    @Override
    @Nullable
    @WebMethod
    public Project findProjectOneByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException{
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        return projectService.findOneByIndex(session.getUserId(), index);
    }

    @Override
    @Nullable
    @WebMethod
    public Project findProjectOneByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException{
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        return projectService.findOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void finishProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException{
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        projectService.finishById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void finishProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException{
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        projectService.finishByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void finishProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException{
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        projectService.finishByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void removeProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "project", partName = "project") @NotNull Project project
    ) throws AccessDeniedException{
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        projectService.remove(session.getUserId(), project);
    }

    @Override
    @WebMethod
    public void removeProjectOneById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException{
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        projectService.removeOneById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeProjectOneByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException{
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        projectService.removeOneByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void removeProjectOneByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException{
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        projectService.removeOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void removeProjectByIdWithTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws AccessDeniedException{
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        projectTaskService.removeProjectById(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public void startProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException{
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        projectService.startById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void startProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException{
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        projectService.startByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void startProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException{
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        projectService.startByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void updateProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AccessDeniedException{
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        projectService.updateById(session.getUserId(), id, name, description);
    }

    @Override
    @Nullable
    @WebMethod
    public void updateProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AccessDeniedException{
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        projectService.updateByIndex(session.getUserId(), index, name, description);
    }

}
