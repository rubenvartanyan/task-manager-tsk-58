package ru.vartanyan.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.repository.model.IProjectRepositoryGraph;
import ru.vartanyan.tm.api.repository.model.ISessionRepositoryGraph;
import ru.vartanyan.tm.api.repository.model.ITaskRepositoryGraph;
import ru.vartanyan.tm.api.repository.model.IUserRepositoryGraph;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.model.IUserServiceGraph;
import ru.vartanyan.tm.dto.User;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.exception.empty.*;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.model.UserGraph;
import ru.vartanyan.tm.repository.model.UserRepositoryGraph;
import ru.vartanyan.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public final class UserServiceGraph extends AbstractServiceGraph<UserGraph>
        implements IUserServiceGraph {

    @NotNull
    public IUserRepositoryGraph getUserRepositoryGraph() {
        return context.getBean(IUserRepositoryGraph.class);
    }

    @NotNull
    public ITaskRepositoryGraph getTaskRepositoryGraph() {
        return context.getBean(ITaskRepositoryGraph.class);
    }

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Override
    @SneakyThrows
    public void add(@Nullable final UserGraph user) {
        if (user == null) throw new NullObjectException();
        IUserRepositoryGraph userRepositoryGraph = getUserRepositoryGraph();
        try {
            userRepositoryGraph.begin();
            userRepositoryGraph.add(user);
            userRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            userRepositoryGraph.rollback();
            throw e;
        } finally {
            userRepositoryGraph.close();
        }
    }


    @SneakyThrows
    @Override
    public void create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login ==  null) throw new EmptyLoginException();
        if (password == null) throw new EmptyPasswordException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
    }

    @SneakyThrows
    @Override
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login ==  null) throw new EmptyLoginException();
        if (password == null) throw new EmptyPasswordException();
        if (email == null) throw new EmptyEmailException();
        @NotNull final UserGraph user = new UserGraph();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        add(user);
    }

    @SneakyThrows
    @Override
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login ==  null) throw new EmptyLoginException();
        if (password == null) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final UserGraph user = new UserGraph();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        add(user);
    }

    @SneakyThrows
    @Override
    public @Nullable UserGraph findByLogin(
            @Nullable final String login
    ) {
        if (login ==  null) throw new EmptyLoginException();
        IUserRepositoryGraph userRepositoryGraph = getUserRepositoryGraph();
        try {
            return userRepositoryGraph.findByLogin(login);
        } finally {
            userRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login ==  null) throw new EmptyLoginException();
        @NotNull final UserGraph user = findByLogin(login);
        user.setLocked(true);
        IUserRepositoryGraph userRepositoryGraph = getUserRepositoryGraph();
        try {
            userRepositoryGraph.begin();
            userRepositoryGraph.update(user);
            userRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            userRepositoryGraph.rollback();
            throw e;
        } finally {
            userRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeByLogin(
            @Nullable final String login
    ) {
        if (login ==  null) throw new EmptyLoginException();
        IUserRepositoryGraph userRepositoryGraph = getUserRepositoryGraph();
        try {
            userRepositoryGraph.begin();
            userRepositoryGraph.removeByLogin(login);
            userRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            userRepositoryGraph.rollback();
            throw e;
        } finally {
            userRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void setPassword(
            @Nullable final String userId, @Nullable final String password
    ) {
        if (userId == null) throw new EmptyIdException();
        if (password == null) throw new EmptyPasswordException();
        @NotNull final UserGraph user = findOneById(userId);
        IUserRepositoryGraph userRepositoryGraph = getUserRepositoryGraph();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) return;
        user.setPasswordHash(hash);
        try {
            userRepositoryGraph.begin();
            userRepositoryGraph.update(user);
            userRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            userRepositoryGraph.rollback();
            throw e;
        } finally {
            userRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login ==  null) throw new EmptyLoginException();
        @NotNull final UserGraph user = findByLogin(login);
        user.setLocked(false);
        IUserRepositoryGraph userRepositoryGraph = getUserRepositoryGraph();
        try {
            userRepositoryGraph.begin();
            userRepositoryGraph.update(user);
            userRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            userRepositoryGraph.rollback();
            throw e;
        } finally {
            userRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (userId == null) throw new EmptyIdException();
        @NotNull final UserGraph user = findOneById(userId);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        IUserRepositoryGraph userRepositoryGraph = getUserRepositoryGraph();
        try {
            userRepositoryGraph.begin();
            userRepositoryGraph.update(user);
            userRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            userRepositoryGraph.rollback();
            throw e;
        } finally {
            userRepositoryGraph.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<UserGraph> entities) {
        if (entities == null) throw new NullObjectException();
        IUserRepositoryGraph userRepositoryGraph = getUserRepositoryGraph();
        try {
            userRepositoryGraph.begin();
            entities.forEach(userRepositoryGraph::add);
            userRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            userRepositoryGraph.rollback();
            throw e;
        } finally {
            userRepositoryGraph.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        IUserRepositoryGraph userRepositoryGraph = getUserRepositoryGraph();
        try {
            userRepositoryGraph.begin();
            userRepositoryGraph.clear();
            userRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            userRepositoryGraph.rollback();
            throw e;
        } finally {
            userRepositoryGraph.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(final @Nullable UserGraph entity) {
        if (entity == null) throw new NullObjectException();
        IUserRepositoryGraph userRepositoryGraph = getUserRepositoryGraph();
        try {
            userRepositoryGraph.begin();
            userRepositoryGraph.removeOneById(entity.getId());
            userRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            userRepositoryGraph.rollback();
            throw e;
        } finally {
            userRepositoryGraph.close();
        }
    }

    @Override
    @SneakyThrows
    public @Nullable List<UserGraph> findAll() {
        IUserRepositoryGraph userRepositoryGraph = getUserRepositoryGraph();
        try {
            return userRepositoryGraph.findAll();
        } finally {
            userRepositoryGraph.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserGraph findOneById(
            @Nullable final String id
    ) {
        if (id == null) throw new EmptyIdException();
        IUserRepositoryGraph userRepositoryGraph = getUserRepositoryGraph();
        try {
            return userRepositoryGraph.findOneById(id);
        } finally {
            userRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneById(
            @Nullable final String id
    ) {
        if (id == null) throw new EmptyLoginException();
        IUserRepositoryGraph userRepositoryGraph = getUserRepositoryGraph();
        try {
            userRepositoryGraph.begin();
            userRepositoryGraph.removeOneById(id);
            userRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            userRepositoryGraph.rollback();
            throw e;
        } finally {
            userRepositoryGraph.close();
        }
    }

}
