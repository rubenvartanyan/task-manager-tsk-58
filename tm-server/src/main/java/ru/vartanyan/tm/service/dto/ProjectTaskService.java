package ru.vartanyan.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.vartanyan.tm.api.repository.dto.IProjectRepository;
import ru.vartanyan.tm.api.repository.dto.ITaskRepository;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.dto.IProjectTaskService;
import ru.vartanyan.tm.api.service.dto.ITaskService;
import ru.vartanyan.tm.dto.Project;
import ru.vartanyan.tm.dto.Task;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.repository.dto.ProjectRepository;
import ru.vartanyan.tm.repository.dto.TaskRepository;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    public ITaskRepository getTaskRepository() {
        return context.getBean(ITaskRepository.class);
    }

    @NotNull
    public IProjectRepository getProjectRepository() {
        return context.getBean(IProjectRepository.class);
    }
    
    @SneakyThrows
    @Override
    public void bindTaskByProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null) throw new EmptyIdException();
        if (projectId == null) throw new EmptyIdException();
        if (taskId == null) throw new EmptyIdException();
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            taskRepository.bindTaskByProjectId(userId, projectId, taskId);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null) throw new EmptyIdException();
        if (projectId == null) throw new EmptyIdException();
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        try {
            return taskRepository.findAllByProjectId(userId, projectId);
        } finally {
            taskRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null) throw new EmptyIdException();
        if (projectId == null) throw new EmptyIdException();
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            taskRepository.begin();
            projectRepository.begin();
            taskRepository.removeAllByProjectId(userId, projectId);
            projectRepository.removeOneByIdAndUserId(userId, projectId);
            taskRepository.commit();
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            projectRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void unbindTaskFromProject(
            @Nullable final String userId, @Nullable final String taskId
    ) {
        if (userId == null) throw new EmptyIdException();
        if (taskId == null) throw new EmptyIdException();
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            taskRepository.unbindTaskFromProjectId(userId, taskId);
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

}
